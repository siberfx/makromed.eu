<?php

use App\Models\Settings\Setting;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('location')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('fax')->nullable();
            $table->string('website')->nullable();
            $table->string('logo')->nullable();
            $table->string('favicon')->nullable();
            $table->string('path')->nullable();
            $table->string('primary_color')->nullable()->default('#2752ec');
            $table->string('primary_color_hover')->nullable()->default('#152a93');
            $table->string('primary_header_bg')->nullable()->default('red');
            $table->string('primary_header_text')->nullable()->default('white');
            $table->boolean('notify_telegram')->nullable()->default(Setting::TELEGRAM_NOTIFY_ON);
            $table->string('telegram_api_key')->default('1954742336:AAE7jgpeX8Rtlugv4Rdxvx2AUPv0xIDUuFk')->nullable();
            $table->string('telegram_chat_id')->default('1001485092550')->nullable()->comment('100 must be added before');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
