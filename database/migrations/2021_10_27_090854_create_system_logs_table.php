<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->string('entity_type');
            $table->unsignedBigInteger('entity_id');
            $table->string('action')->nullable();
            $table->text('description')->nullable();
            $table->dateTime('event_date')->useCurrent();
            $table->string('ip_address', 45);
            $table->timestamps();
            
            $table->index(['entity_type', 'entity_id'], 'system_logs_entity_type_entity_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_logs');
    }
}
