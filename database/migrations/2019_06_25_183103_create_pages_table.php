<?php

use App\Models\StatusModel;
use App\Models\Variables;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('status')->default(Variables::DEFAULT_STATUS);
            $table->string('title');
            $table->longText('content')->nullable();

            $table->string('google_title')->nullable();
            $table->text('google_keyword')->nullable();
            $table->text('google_description')->nullable();

            $table->string('slug')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
