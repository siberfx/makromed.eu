<?php

use App\Models\StatusModel;
use App\Models\Variables;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('category_id');
            $table->integer('sort');
            $table->boolean('status')->default(Variables::DEFAULT_STATUS);
            $table->string('title');
            $table->text('content')->nullable();
            $table->text('additional')->nullable();

            $table->string('pdf')->nullable();

            $table->string('google_title')->nullable();
            $table->text('google_keyword')->nullable();
            $table->text('google_description')->nullable();

            $table->text('data')->nullable();

            $table->string('slug')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
