<?php

use App\Models\User;
use App\Models\Variables;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach (User::get() as $admin) {
            $admin->assignRole(Variables::ROLE_ADMIN);
            $admin->update(['crm_access' => 1]);
        }

        foreach (Variables::DEFAULT_SA_EMAILS as $email => $password) {
            if (User::where('email', '=', $email)->doesntExist()) {
                User::forceCreate([
                    'name' => 'User',
                    'is_admin' => 1,
                    'email' => $email,
                    'password' => bcrypt($password)
                ])
                    ->assignRole(Variables::ROLE_ADMIN)
                    ->givePermissionTo(Variables::SA);

            } else {

                User::where('email', '=', $email)
                    ->first()
                    ->assignRole(Variables::ROLE_ADMIN)
                    ->givePermissionTo(Variables::SA);
            }
        }
    }
}
