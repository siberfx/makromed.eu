<?php

use App\Models\Category;
use App\Models\Variables;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Category::truncate();

        $data = [
            [
                "id" => 1,
                "sort" => 1,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "Анестезиология и Pеанимация",
                "content" => "<p>Anesthesia Units </p>",
                "slug" => "anesthesia",
                "created_at" => "2018-03-05 20:41:09",
                "updated_at" => now()
            ],
            [
                "sort" => 2,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "Респиратори",
                "content" => "",
                "slug" => "ventilators",
                "created_at" => "2017-10-02 21:46:12",
                "updated_at" => now()
            ],
            [
                "sort" => 3,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "Пациентни монитори",
                "content" => "",
                "slug" => "patient-monitoring",
                "created_at" => "2017-10-02 21:46:21",
                "updated_at" => now()
            ],
            [
                "sort" => 4,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "Дефибрилатори",
                "content" => "<p>Defibrillator</p>",
                "slug" => "defibrillator",
                "created_at" => "2017-10-02 21:46:30",
                "updated_at" => now()
            ],
            [
                "sort" => 5,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "Eндоскоп",
                "content" => "",
                "slug" => "endoscopes",
                "created_at" => "2017-10-02 21:46:37",
                "updated_at" => now()
            ],
            [
                "sort" => 7,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "Електрокоагулация",
                "content" => "",
                "slug" => "electrosurgery",
                "created_at" => "2017-10-02 21:46:44",
                "updated_at" => now()
            ],
            [
                "sort" => 8,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "Eхокардиография",
                "content" => "<p>EKG ECG&nbsp;</p>",
                "slug" => "ecg-ekg",
                "created_at" => "2017-10-02 21:47:08",
                "updated_at" => now()
            ],
            [
                "sort" => 9,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "Инфузионни и Спринцовъчни Помпи",
                "content" => "",
                "slug" => "infusion-and-syringe-pumps",
                "created_at" => "2017-10-02 21:47:16",
                "updated_at" => now()
            ],
            [
                "sort" => 10,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "Охлаждане на пациента",
                "content" => "",
                "slug" => "patient-heater-cooler",
                "created_at" => "2017-10-02 21:47:24",
                "updated_at" => now()
            ],
            [
                "sort" => 11,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "ЕЕГ / ЕМГ",
                "content" => "<p>EEG and EMG Unit</p>",
                "slug" => "eeg-emg",
                "created_at" => "2018-06-17 21:07:22",
                "updated_at" => now()
            ],
            [
                "sort" => 12,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "Ултразвуков скенер",
                "content" => "<p>-</p>",
                "slug" => "ultrasound",
                "created_at" => "2018-06-23 17:27:23",
                "updated_at" => now()
            ],
            [
                "sort" => 13,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "Aксесоари",
                "content" => "<p>Accesories - Spare Parts</p>",
                "slug" => "accessories",
                "created_at" => "2017-10-05 04:08:34",
                "updated_at" => now()
            ],
            [
                "sort" => 6,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "Медицинска бръснара",
                "content" => "<p>-</p>",
                "slug" => "shaver",
                "created_at" => "2018-06-23 17:29:58",
                "updated_at" => now()
            ],
            [
                "sort" => 0,
                "status" => Variables::DEFAULT_STATUS,
                "title" => "Диализа",
                "content" => '<p><span style="font-weight: 700 !important;">Dialysis Machine</span></p><p>This machine is used on the kidneys to help flush them out. Looks very good in a patients room and emergency rooms. Very easy to turn on and make look activated.</p>',
                "slug" => "dialysis",
                "created_at" => "2019-05-22 10:05:25",
                "updated_at" => now()
            ],
        ];

        foreach ($data as $category) {
            Category::create($category);
        }
    }
}
