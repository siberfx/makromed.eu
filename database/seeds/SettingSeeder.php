<?php

use App\Models\Settings\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'name' => "MAKROMED DIŞ TİCARET LİMİTED ŞİRKETİ",
            'mobile' => "+90-541-650-1740",
            'phone' => "+90-216-700-2464",
            'fax' => "+90-216-470-0564",
            'location' => "Bağlarbaşı Mah. Sakarya Sok. No.13, 34844 - Maltepe İSTANBUL / TÜRKİYE",
            'email' => 'info@macromed.eu',
            'logo' => 'small-logo.png',
            'path' => 'siberfx.jpg',
            'favicon' => 'favicon.png',
            'primary_color' => 'darkblue',
            'primary_color_hover' => 'blue',
            'primary_header_bg' => 'black',
            'primary_header_text' => 'white',

        ]);
    }
}
