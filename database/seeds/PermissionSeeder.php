<?php

use App\Models\Variables;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create roles
        foreach (Variables::$fullRoles as $role) {
            if (Role::where('name', '=', $role)->doesntExist()) {
                Role::create(['name' => $role]);
            }
        }
        //create permissions
        foreach (Variables::$fullPermissions as $permission => $roles) {

            if (Permission::where('name', $permission)->doesntExist()) {
                //create permission
                $permissionInstance = Permission::insert(['name' => $permission, 'guard_name' => Variables::GUARD_NAME]);
                //authorize roles to those permissions
                foreach ($roles as $role) {
                    Role::where('name', $role)->first()->givePermissionTo($permissionInstance);
                }
            }
        }
    }
}
