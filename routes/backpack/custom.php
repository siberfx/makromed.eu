<?php

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes

    Route::crud('user', 'UserCrudController');

    // Notification templates
    Route::crud('notification-templates', 'NotificationTemplateController');
    Route::post('notification-templates/list-model-variables', 'NotificationTemplateController@listModelVariables')->name('listModelVariables');

    Route::get('edit-account-info', 'MyAccountController@getAccountInfoForm')->name('backpack.account.info');
    Route::post('edit-account-info', 'MyAccountController@postAccountInfoForm')->name('backpack.account.info.store');
    Route::post('change-password', 'MyAccountController@postChangePasswordForm')->name('backpack.account.password');

    Route::crud('setting', 'SettingController');
    Route::crud('system-logs', 'SystemLogController');
    Route::crud('product', 'ProductCrudController');
    Route::crud('category', 'CategoryCrudController');
}); // this should be the absolute last line of this file