<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;

$locale = ( in_array(Request::segment(1), config('app.locales')) ) ? Request::segment(1) : "en";
App::setLocale($locale);

Route::get('/', [HomeController::class, 'index']);

Route::group(['prefix' => $locale], function() {

    Route::get('/', [HomeController::class, 'index'])->name('index');

    Route::get(__('routes.index'), [HomeController::class, 'index'])->name('index');
    Route::get(__('routes.about'), [HomeController::class, 'company'])->name('about');
    Route::get(__('routes.policy'), [HomeController::class, 'policy'])->name('policy');
    Route::get(__('routes.contact'), [HomeController::class, 'contact'])->name('contact');

    Route::group(['prefix' => __('routes.page')], function() {
        Route::get('/{slug}', [HomeController::class, 'block'])->name('block.index');
    });

    Route::group(['prefix' => __('routes.products')], function() {
        Route::get('/', [ProductController::class, 'index'])->name('product.index');
        Route::get('/{category:slug}/{product:slug?}', [ProductController::class, 'show'])->name('product.show');
    });

});
