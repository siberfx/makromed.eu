<?php

use App\Models\Variables;
use Illuminate\Support\Carbon;

$tag  = exec('git describe --tags --abbrev=0');

if(empty($tag)) {
    $tag = '0.0.1 beta';
}

$hash = trim(exec('git log --pretty="%h" -n1 HEAD'));

$dateStartInstance = Carbon::parse(trim(exec('git log --pretty=format:%ad | tail -1')));
$dateUpdateInstance = Carbon::parse(trim(exec('git log -n1 --pretty=%ci HEAD')));

$dateStart = Variables::mutateDate($dateStartInstance);
$dateUpdate = Variables::mutateDate($dateStartInstance);

$age = $dateStartInstance->diffInDays($dateUpdateInstance);

$commitCount = trim(exec('git rev-list HEAD --count'));

return [
    'tag' => $tag,
    'date' => $dateUpdate,
    'projectAge' => $age,
    'hash' => $hash,
    'string' => sprintf('%s <small>( Tarih: %s, <em>Hash: %s, commits: %s </em>)</small>',$tag, $dateUpdate .' <small>('. $age .' days)</small>', $hash, $commitCount),
];
