<?php

namespace App\Console\Commands;

use App\Mail\PlainMail;
use App\Models\SentEmail;
use App\Models\Settings\Setting;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-emails:now';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending emails command';

    protected array $emails = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

         // $this->emails = SentEmail::whereSent(SentEmail::OPEN)->get();
    }

    /**
     * @return bool
     */
    public function handle()
    {
        foreach ($this->emails as $single) {

            $cc = app()->environment('local')
                ? 'info@siberfx.com'
                : $single->cc;

            $to = explode(',', $single->to);

            $mail = Mail::to($to)->cc($cc);

            $mail->send(new PlainMail($single));

            if (count(Mail::failures()) > 0) {
                $single->update(['sent' => SentEmail::FAILED]);
            } else {
                $single->update(['sent' => SentEmail::SUCCEED, 'planned_at' => now()]);
            }
        }

        return true;
    }
}
