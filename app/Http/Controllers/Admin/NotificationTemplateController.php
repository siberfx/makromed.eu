<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests\NotificationTemplateRequest;
use App\Models\NotificationTemplate;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\JsonResponse;

/**
 * Class NotificationTemplateController
 * @package App\Http\Controllers\User
 * @property-read CrudPanel $crud
 */
class NotificationTemplateController extends CrudController
{
    use ListOperation;
    use UpdateOperation;
    use ShowOperation;

    public function setup()
    {
        CRUD::setModel(NotificationTemplate::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/notification-templates');
        CRUD::setEntityNameStrings('', 'E-posta Şablonları');

    }

    /**
     * defining the roles
     */
    public function setPermissions()
    {
        // Get authenticated user
        $user = backpack_user();
        $prefix = 'template';

        // Allow list access
        if ($user->can($prefix . '-list')) {
            CRUD::allowAccess('list');
        }
        // Allow show access
        if ($user->can($prefix . '-preview')) {
            CRUD::allowAccess('show');
        }
        // Allow create access
        if ($user->can($prefix . '-create')) {
            CRUD::allowAccess('create');
        }
        // Allow update access
        if ($user->can($prefix . '-update')) {
            CRUD::allowAccess('update');
        }
        // Allow delete access
        if ($user->can($prefix . '-delete')) {
            CRUD::allowAccess('delete');
        }

    }

    public function listModelVariables(NotificationTemplateRequest $request): ?JsonResponse
    {
        $modelClass = 'App\\Models\\' . $request->input('model');

        if (class_exists($modelClass)) {
            $model = new $modelClass;

            return response()->json($model->notificationVars);
        }

        return null;
    }

    /**
     * Check variables in body to match the available variables from the model
     * @param  $request
     * @return boolean
     */
    public function checkModelVariables($request): bool
    {
        preg_match_all('/(\{{2}\s?(.*?)\s?\}{2})/mi',
            $request->body,
            $out, PREG_PATTERN_ORDER);

        if (count(array_diff($out[2], $this->getModelVariables($request->model))) > 0) {
            return false;
        }
        return true;
    }

    /**
     * Get model variables available to use in an email template
     * @param string $modelName
     * @return array
     */
    public function getModelVariables(string $modelName): array
    {
        $modelClass = 'App\\Models\\' . $modelName;

        if (class_exists($modelClass)) {
            $model = new $modelClass;
        }

        return $model->notificationVars;
    }

    protected function setupListOperation()
    {
        CRUD::addColumns($this->getColumns());
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return [
            [
                'name' => 'name',
                'label' => __('Başlık'),
            ],
            [
                'name' => 'updated_at',
                'label' => __('backend.updated_at'),
                'type' => 'datetime',
            ],

        ];
    }

    protected function setupUpdateOperation()
    {

        $this->setupCreateOperation();

    }

    protected function setupCreateOperation()
    {
        CRUD::setValidation(NotificationTemplateRequest::class);

        CRUD::addFields($this->getFields());
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return [
            [
                'name' => 'name',
                'label' => trans('name'),
                'type' => 'text',
            ],
            [
                'name' => 'slug',
                'label' => trans('slug'),
                'type' => 'slug',
                // 'attributes' => ['disabled' => 'disabled']
            ],
            [
                'name' => 'model',
                'label' => trans('Model'),
                'type' => 'select2_from_array_notification_template_model',
                'options' => NotificationTemplate::$availableModels,
            ],
            [
                'name' => 'body',
                'label' => trans('İçerik Gövdesi'),
                'type' => 'textarea',
                'attributes' => [
                    'rows' => 10,
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-9 col-xs-12'
                ]
            ],
            [
                'name' => 'notification_list_variables',
                'label' => 'Kullanılabilir parametreler',
                'type' => 'notification_list_variables',
                'wrapperAttributes' => [
                    'class' => 'form-group available-variables col-md-3 col-xs-12'
                ]
            ],
        ];
    }

}
