<?php

namespace App\Http\Controllers\Admin;

use Alert;
use App\Http\Requests\AccountInfoRequest;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller;
use Hash;
use Illuminate\View\View;

class MyAccountController extends Controller
{
    protected $data = [];

    public function __construct()
    {
        $this->middleware(backpack_middleware());
    }

    /**
     * @return Application|Factory|View
     */
    public function getAccountInfoForm()
    {
        $this->data['title'] = trans('backpack::base.my_account');
        $this->data['user'] = $this->guard()->user();

        return view(backpack_view('my_account'), $this->data);
    }

    /**
     * @param AccountInfoRequest $request
     * @return RedirectResponse
     */
    public function postAccountInfoForm(AccountInfoRequest $request)
    {

        $data = [
            'email_notifications' => ($request->has('email_notifications')) ? 1 : 0,
            'name' => $request->name,
            'email' => $request->email,
        ];

        $result = $this->guard()->user()->update($data);

        if ($result) {
            Alert::success(trans('backpack::base.account_updated'))->flash();
        } else {
            Alert::error(trans('backpack::base.error_saving'))->flash();
        }

        return redirect()->back();
    }

    /**
     * @param ChangePasswordRequest $request
     * @return RedirectResponse
     */
    public function postChangePasswordForm(ChangePasswordRequest $request)
    {
        $user = $this->guard()->user();
        $user->password = Hash::make($request->new_password);

        if ($user->save()) {
            Alert::success(trans('backpack::base.account_updated'))->flash();
        } else {
            Alert::error(trans('backpack::base.error_saving'))->flash();
        }

        return redirect()->back();
    }

    /**
     * @return Guard|StatefulGuard
     */
    protected function guard()
    {
        return backpack_auth();
    }
}
