<?php

namespace App\Http\Controllers\Admin\Traits;

use App\Models\Variables;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

trait PermissionTrait
{

    public function isPermitted(string $prefix = null, bool $partial = false): bool
    {

        if (backpack_user()->hasPermissionTo(Variables::SA)) {
            return true;
        }

        if ($partial) {
            $this->isAllowed($prefix, 'access');
            $this->isAllowed($prefix, 'show');

        } else {
            $this->isAllowed($prefix, 'access');
            $this->isAllowed($prefix, 'create');
            $this->isAllowed($prefix, 'edit');
            $this->isAllowed($prefix, 'delete');
            $this->isAllowed($prefix, 'show');
        }

        return true;

    }

    public function isAllowed($prefix, $action)
    {
        $actionName = $action == 'access' ? 'list' : $action;

        if (!backpack_user()->hasPermissionTo($prefix.'_'.$action)) {
            CRUD::denyAccess([$actionName]);
        }

    }

    // for admin related checks
    public function isPermittedChangeSettings()
    {
        if (! backpack_user()->hasPermissionTo(Variables::SA) || ! backpack_user()->hasRole(Variables::ROLE_ADMIN)) {
            CRUD::denyAccess(['list']);
            CRUD::denyAccess(['show']);
            CRUD::denyAccess(['create']);
            CRUD::denyAccess(['update']);
        }

    }
}
