<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Traits\PermissionTrait;
use App\Http\Requests\SystemLogRequest;
use App\Models\SystemLog;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SystemLogController
 * @package App\Http\Controllers\User
 * @property-read CrudPanel $crud
 */
class SystemLogController extends CrudController
{
    use ListOperation;
    use ShowOperation;
    use PermissionTrait;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(SystemLog::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/system-logs');
        CRUD::setEntityNameStrings('', 'İşlem Kayıtları');

        $this->isPermittedChangeSettings();

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns($this->getColumns());

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SystemLogRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function getColumns(): array
    {
        return [
            [
                'name' => 'entity',
                'label' => 'Modül',
                'type' => 'closure',
                'function' => function($entry) {
                    return substr($entry->entity_type, 11);
                },
                'searchLogin' => false,
            ],
            [
                'name' => 'admin',
                'label' => 'İşlemi gerçekleştiren',
                'type' => 'closure',
                'function' => function($entry) {
                    return $entry->admin->name;
                },
                'searchLogin' => false,
            ],
            [
                'name' => 'description',
                'label' => 'Açıklama',
            ],
            [
                'name' => 'action',
                'label' => 'Aksiyon',
            ],
            [
                'name' => 'ip_address',
                'label' => 'IP Adresi',
            ],
            [
                'name' => 'event_date',
                'label' => 'İşlem Tarihi',
                'type' => 'datetime'
            ],
        ];
    }
}
