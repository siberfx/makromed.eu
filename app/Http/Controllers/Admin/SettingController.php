<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Traits\PermissionTrait;
use App\Http\Requests\SettingRequest;
use App\Models\Settings\Setting;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SettingController
 * @package App\Http\Controllers\User
 * @property-read CrudPanel $crud
 */
class SettingController extends CrudController
{
    use ListOperation;
    use UpdateOperation;
    use ShowOperation;
    use PermissionTrait;


    public function setup()
    {
        CRUD::setModel(Setting::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/setting');
        CRUD::setEntityNameStrings('', 'Ayarlar');

        $this->isPermittedChangeSettings();
    }

    protected function setupListOperation()
    {
        CRUD::addColumns($this->getColumns());
    }

    protected function setupCreateOperation()
    {
        CRUD::setValidation(SettingRequest::class);

        CRUD::addFields($this->getFields());
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return [
            [
                'label' => 'Logo',
                'name' => 'logo',
                'type' => 'image',
            ],
            [
                'label' => 'Favicon',
                'name' => 'favicon',
                'type' => 'image',
            ],
            [
                'label' => 'Site Adı',
                'name' => 'name',
                'type' => 'text',
            ],
            [
                'label' => 'E-posta',
                'name' => 'email',
                'type' => 'text',
            ],
            [
                'label' => 'Birincil Renk',
                'name' => 'primary_color',
                'type' => 'colorbox',
            ],
            [
                'label' => 'Birincil Renk - Aktif',
                'name' => 'primary_color_hover',
                'type' => 'colorbox',
            ],
            [
                'label' => 'Üstmenü arka zemin rengi',
                'name' => 'primary_header_bg',
                'type' => 'colorbox',
            ],
            [
                'label' => 'Üst menü yazı rengi',
                'name' => 'primary_header_text',
                'type' => 'colorbox',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getFields(): array
    {

        return [
            [
                'label' => 'Site Adı',
                'name' => 'name',
                'type' => 'text',
                'tab' => 'Genel'
            ],
            [
                'label' => 'Lokasyon',
                'name' => 'location',
                'type' => 'address_algolia',
                'tab' => 'Genel'
            ],
            [
                'label' => 'Telefon',
                'name' => 'phone',
                'type' => 'text',
                'tab' => 'Genel'
            ],
            [
                'label' => 'Website Adresi',
                'name' => 'website',
                'type' => 'text',
                'tab' => 'Genel'
            ],[
                'label' => 'Logo',
                'name' => 'logo',
                'type' => 'browse',
                'hint' => 'Logo optimal boyut "264x64"px',
                'tab' => 'Görseller'
            ],
            [
                'label' => 'Favicon',
                'name' => 'favicon',
                'type' => 'browse',
                'hint' => '16x16 32x32 50x50 boyutlarından biri kullanılmalıdır',
                'tab' => 'Görseller'
            ],
            [
                'label' => 'Giriş Sağ Resmi',
                'name' => 'path',
                'type' => 'browse',
                'hint' => 'Resim öznitelikleri: 1549x1440px olmalıdır!',
                'tab' => 'Görseller'
            ],
            [
                'label' => 'Birincil Renk',
                'name' => 'primary_color',
                'type'    => 'color',
                'default' => '#000000',
                'tab' => 'Renkler'
            ],
            [
                'label' => 'Birincil Renk - Aktif',
                'name' => 'primary_color_hover',
                'type'    => 'color',
                'default' => '#000000',
                'tab' => 'Renkler'
            ],
            [
                'label' => 'Üst menü arka zemin',
                'name' => 'primary_header_bg',
                'type'    => 'color',
                'default' => '#000000',
                'tab' => 'Renkler'
            ],
            [
                'label' => 'Üst menü yazı rengi',
                'name' => 'primary_header_text',
                'type'    => 'color',
                'default' => '#000000',
                'tab' => 'Renkler'
            ],
            [
                'label' => 'Telegram bildirimlerini aç',
                'name' => 'notify_telegram',
                'type' => 'toggle',
                'hint' => 'Bu özellik açık olması halinde bildirimler telegrama gönderilecektir aksi halde e-posta olarak varsayılan şekilde ayarlıdır.',
                'tab' => 'Bildirim Ayarları'
            ],
            [
                'label' => 'E-posta',
                'name' => 'email',
                'type' => 'email',
                'tab' => 'Bildirim Ayarları'
            ],
            [
                'label' => 'API Key',
                'name' => 'telegram_api_key',
                'hint' => '@BotFather `dan bota ait API key',
                'tab' => 'Bildirim Ayarları'
            ],
            [
                'label' => 'Chat ID',
                'name' => 'telegram_chat_id',
                'hint' => 'Telegram mesajını alacak olan kanal ya da kişi ID\'si',
                'tab' => 'Bildirim Ayarları'
            ],
        ];
    }
}
