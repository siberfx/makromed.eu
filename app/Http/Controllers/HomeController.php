<?php

namespace App\Http\Controllers;

use App\Models\Page;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $about = Page::first();

        return view('home.index', compact('about'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function company()
    {
        $about = Page::first();


        return view('corporate.index', compact('about'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function policy()
    {
        $about = Page::whereId(2)->first();


        return view('policy.index', compact('about'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        $about = Page::whereId(2)->first();


        return view('contact.index', compact('about'));
    }


}
