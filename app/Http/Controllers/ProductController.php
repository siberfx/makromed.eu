<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $relation = Category::with('products')->active();
        $categories = $relation->paginate(6);

        return view('products.index', compact('categories'));
    }

    /**
     * @param Category $category
     * @param Product|null $product
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Category $category, ?Product $product)
    {
        $categories = Category::active()->get();

        $category->load('products');

        if (is_null($product->title)) {
            return view('product.category', compact('category', 'categories'));
        }

        return view('product.show', compact('category', 'categories'));

    }
}
