<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'logo' => 'required',
             'name' => 'required',
             'email' => 'required',
             'primary_color' => 'required',
             'primary_color_hover' => 'required',
             'primary_header_bg' => 'required',
             'primary_header_text' => 'required',
             'favicon' => 'required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [

        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'logo.required' => 'Bir logo olması zorunludur',
            'favicon.required' => 'Bir favori ikon olması zorunludur',
            'name.required' => 'Başlık boş bırakılamaz',
            'email.required' => 'E-posta boş bırakılamaz',
        ];
    }
}
