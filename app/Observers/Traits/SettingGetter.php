<?php

namespace App\Observers\Traits;

use App\Models\Settings\Setting;

trait SettingGetter
{
    public $setting;

    public function __construct()
    {
        $this->setting = Setting::first();
    }

}
