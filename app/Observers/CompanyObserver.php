<?php

namespace App\Observers;

use App\Models\Company;

class CompanyObserver
{
    /**
     * @param Company $company
     */
    public function created(Company $company)
    {
        //
    }

    /**
     * @param Company $company
     */
    public function updated(Company $company)
    {
        //
    }

    /**
     * @param Company $company
     * @return bool
     */
    public function deleting(Company $company)
    {

    }
    /**
     * @param Company $company
     */
    public function deleted(Company $company)
    {
        //
    }

    /**
     * Handle the company "restored" event.
     *
     * @param Company $company
     * @return void
     */
    public function restored(Company $company)
    {
        //
    }

    /**
     * @param Company $company
     */
    public function forceDeleted(Company $company)
    {

    }
}
