<?php

namespace App\Notifications;

use App\Models\Settings\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramMessage;
use Siberfx\AuthenticationLogger\Models\AuthLogger;

class FailedLoginAttempt extends Notification implements ShouldQueue
{
    use Queueable;

    public AuthLogger $AuthLogger;
    public $setting;

    public function __construct(AuthLogger $AuthLogger)
    {
        $this->AuthLogger = $AuthLogger;
        $this->setting = Setting::first();
    }

    public function via($notifiable)
    {
        return ['telegram'];
    }

    public function toTelegram($notifiable): TelegramMessage
    {

        $variables = [
            'account' => $notifiable->email,
            'time' => $this->AuthLogger->login_at,
            'ip_address' => $this->AuthLogger->ip_address,
            'browser' => $this->AuthLogger->user_agent,
            'location' => $this->AuthLogger->location,
        ];

        return TelegramMessage::create()
            // Optional recipient user id.
//            ->to($notifiable->telegram_user_id)
            ->to('-'.$this->setting->telegram_chat_id)
            // Markdown supported.
            ->content(config('app.url').' Başarısız giriş denemesi : ' .$notifiable->email . ' '. $this->AuthLogger->login_at . ' ' . $this->AuthLogger->ip_address);

    }

}
