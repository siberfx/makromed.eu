<?php

namespace App\Helpers;

use App\Models\Settings\Setting;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramHelper
{
    /**
     * @param $message
     * @param array $attributes
     * @return bool
     */
    public static function render($message, array $attributes = []): bool
    {
        $setting = Setting::first();


        Telegram::setAsyncRequest(true)
            ->sendMessage([
            'chat_id' => '-'.$setting->telegram_chat_id,
            'parse_mode' => 'HTML',
            'text' => $message,
        ]);

        return true;
    }

    /**
     * @return string
     */
    public static function whoAmI(): string
    {
        $response = Telegram::getMe();

        $botId = $response->getId();
        $firstName = $response->getFirstName();
        $username = $response->getUsername();

        return $botId.' '. $firstName. ' '. $username;
    }
}
