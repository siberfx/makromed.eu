<?php

namespace App\Helpers;

use App\Models\NotificationTemplate;
use App\Models\SentEmail;
use App\Models\Settings\Setting;

class EmailQueueHelper
{

    public $model;
    public string $to;
    public string $cc;
    public $setting;
    public $template;

    /**
     * @param $model
     * @param $slug
     * @param null $subject
     * @param array $to
     * @param string $cc
     */
    public function __construct($model, $slug, $subject = null, array $to = [], string $cc = 'info@siberfx.nl')
    {

        $this->setting = Setting::first();

        $this->template = NotificationTemplate::where('slug', $slug)->first();
        $this->to = count($to) > 0 ? collect($to)->implode(',') : $this->setting->email;
        $this->cc = ($this->telegramOn()) ? '' : ($this->setting->email ?? 'info@siberfx.com');
        $this->model = $model;
        $this->subject = $subject ?? 'Hizmet Yenileme Bildirimi';

        $this->build();

    }

    public function build()
    {
        $variables = $this->model->notificationVariables();

        $patterns = collect($variables)->map(fn($variable, $key) => '/(\{{2}\s?'.$key.'\s?\}{2})/mi');

        $body = preg_replace($patterns->toArray(), $variables, $this->template->body);

        SentEmail::create([
            'to' => $this->to,
            'cc' => $this->cc,
            'subject' => $this->subject,
            'body' => $body,
            'sent' => SentEmail::OPEN,
            'planned_at' => now()->addMinutes(SentEmail::EMAIL_DELAY),
            'created_at' => now(),
        ]);

        if ($this->telegramOn()) {
            TelegramHelper::render($this->subject);
        }

        return true;
    }

    private function telegramOn(): bool
    {
        return (Setting::TELEGRAM_NOTIFY_ON == $this->setting->notify_telegram);
    }
}
