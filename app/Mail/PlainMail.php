<?php

namespace App\Mail;

use App\Models\Settings\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PlainMail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct($model)
    {
        $this->body = $model->body;
        $this->subject = $model->subject;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $body = $this->body;

        return $this
            ->from('noreply@atlantik.web.tr', Setting::first()->name)
            ->replyTo('info@atlantikyazilim.com', 'Atlantik Yazılım')
            ->view('email.custom.layout', compact('body'));
    }
}
