<?php

namespace App\Mail;

use App\Models\NotificationTemplate;
use App\Models\SentEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificationTemplateMail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct($to, $cc, $subject, $body)
    {
        $this->to = $to;
        $this->cc = $cc;
        $this->body = $body;
        $this->subject = $subject ?? 'Hizmet Yenileme Bildirimi';

    }

    /**
     * @return NotificationTemplateMail
     */
    public function build(): NotificationTemplateMail
    {
        $variables = $this->model->notificationVariables();

        $patterns = collect($variables)->map(fn($variable, $key) => '/(\{{2}\s?'.$key.'\s?\}{2})/mi');

        $body = preg_replace($patterns->toArray(), $variables, $this->template->body);

        SentEmail::create([
            'to' => $this->to[0]['address'] ?? '-',
            'subject' => $this->subject,
            'body' => $body,
            'created_at' => now(),
        ]);


        return $this->view('email.notification_template.layout', compact('body'));
    }
}
