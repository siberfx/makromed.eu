<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * @mixin IdeHelperPage
 */
class Page extends BaseModel
{
    use HasTranslations;

    protected $translatable = ['title', 'content'];

    protected $table = 'pages';

    protected $fillable = [
        'status',
        'title',
        'content',
        'data',
        'slug',
    ];

}
