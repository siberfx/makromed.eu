<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\SentEmail
 *
 * @property int $id
 * @property string|null $to
 * @property string|null $cc
 * @property string|null $subject
 * @property string|null $body
 * @property int|null $sent
 * @property string|null $planned_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|SentEmail newModelQuery()
 * @method static Builder|SentEmail newQuery()
 * @method static Builder|SentEmail query()
 * @method static Builder|SentEmail whereBody($value)
 * @method static Builder|SentEmail whereCc($value)
 * @method static Builder|SentEmail whereCreatedAt($value)
 * @method static Builder|SentEmail whereId($value)
 * @method static Builder|SentEmail wherePlannedAt($value)
 * @method static Builder|SentEmail whereSent($value)
 * @method static Builder|SentEmail whereSubject($value)
 * @method static Builder|SentEmail whereTo($value)
 * @method static Builder|SentEmail whereUpdatedAt($value)
 * @mixin Eloquent
 * @mixin IdeHelperSentEmail
 */
class SentEmail extends BaseModel
{
    const
        OPEN = 0,
        SUCCEED = 1,
        ABORTED = 2,
        FAILED = 3,

        EMAIL_DELAY = 15;

    public static array $colors = [
        self::OPEN => 'orange',
        self::SUCCEED => 'green',
        self::ABORTED => 'gray',
        self::FAILED => 'red',
    ];
    public static array $statuses = [
        self::OPEN => 'Açık',
        self::SUCCEED => 'Gönderildi',
        self::ABORTED => 'İptal Edildi',
        self::FAILED => 'Başarısız Oldu',
    ];

    protected $table = 'sent_emails';

    protected $guarded = [];

}
