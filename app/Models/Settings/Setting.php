<?php

namespace App\Models\Settings;

use App\Models\BaseModel;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

/**
 * @mixin IdeHelperSetting
 */
class Setting extends BaseModel
{
    const TELEGRAM_NOTIFY_ON = 1;

    protected $table = 'settings';

    protected $guarded = [];

    protected $appends = [
        'mutate_primary_color',
        'mutate_primary_color_hover',
        'mutate_primary_header_bg',
    ];

    public function getMutatePrimaryColorAttribute(): string
    {
        return $this->primary_color ? mb_substr(explode('rgb(', $this->primary_color)[1], 0, -1) : '245, 246, 252';
    }

    public function getMutatePrimaryColorHoverAttribute(): string
    {
        return $this->primary_color_hover ? mb_substr(explode('rgb(', $this->primary_color_hover)[1], 0, -1) : '117, 19, 93';
    }


}
