<?php

namespace App\Models;

use App\Models\Traits\AuthLoggable;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

/**
 * @mixin IdeHelperUser
 */
class User extends Authenticatable
{
    use Notifiable, CrudTrait, HasRoles, AuthLoggable;

    protected $table = 'users';

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'is_admin'
    ];

    /**
     * @var string[]
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'full_name'
    ];


    /**
     * @param string $role
     * @return User[]|Builder[]|Collection
     */
    public static function getUsers(string $role = Variables::ROLE_ADMIN)
    {
        $role = Role::where('name', Variables::ROLE_ADMIN)->first();

        return self::whereHas('roles', function($query) use ($role){
            $query->where('roles.id', $role->id);
        })->get();
    }

    /**
     * @param string $role
     * @return array
     */
    public static function getUsersArray(string $role = Variables::ROLE_ADMIN): array
    {
        $role = Role::where('name', Variables::ROLE_ADMIN)->first();

        return self::whereHas('roles', function($query) use ($role){
            $query->where('roles.id', $role->id);
        })->get()->pluck('name', 'id')->toArray();
    }

    /**
     * @var array
     */
    public array $notificationVars = [
        'full_name',
        'email',
        'phone',
    ];

    /**
     * @return array
     */
    public function notificationVariables(): array
    {
        return [
            'full_name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
        ];
    }

    /**
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return sprintf('%s', $this->name);
    }

}
