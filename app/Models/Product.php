<?php

namespace App\Models;

use App\Utils\Helpers\ImageHelper;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Storage;

/**
 * @mixin IdeHelperProduct
 */
class Product extends BaseModel
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use Sluggable;
    use SluggableScopeHelpers;
    use HasTranslations;

    protected $translatable = ['title', 'content', 'additional'];

    protected $table = 'products';

    protected $fillable = [
        'category_id',
        'sort',
        'status',
        'title',
        'content',
        'additional',
        'slug',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($model) {

            // Delete product images
            $disk = 'products';

            foreach ($model->images as $image) {
                // Delete image from disk
                if (Storage::disk($disk)->has($image->name)) {
                    Storage::disk($disk)->delete($image->name);
                }
                // Delete image from db
                $image->delete();
            }
        });
    }


    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function property(): array
    {
        $property = $this->properties()->where('product_id', $this->id)->first();

        return [
            'part_number' => $property->part_number,
            'manufacturer' => $property->manufacturer,
            'brand' => $property->brand,
            'product_number' => $property->product_number,
            'quantity' => $property->quantity,
            'conditions' => $property->conditions,
            'dimentions' => $property->dimentions,
            'weight' => $property->weight,
        ];
    }


    public function getImage(): string
    {
        $get = $this->images->count() > 0 ? '/uploads/products/'.$this->images()->first()->name : asset(config('app.default_image'));
        return $get;
    }


    public function getCategory()
    {
        return $this->category()->where('id', $this->category_id)->first()->title;
    }


    public function scopeActive($query)
    {
        return $query->where('status', StatusModel::ACTIVE)
            ->orderBy('sort', 'asc');
    }


    public function setPathAttribute($value)
    {
        $attribute_name = "path";
        $disk = "uploads";
        $path = "portfolio";

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            Storage::disk($disk)->delete($this->{$attribute_name});

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image')) {
            // 0. Saving image
            $this->attributes[$attribute_name] = ImageHelper::decodeAsFile($disk, $value, $path);
        }
    }

}
