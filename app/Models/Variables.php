<?php

namespace App\Models;

use App\Models\Traits\DateMutator;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Variables
 *
 * @method static Builder|Variables newModelQuery()
 * @method static Builder|Variables newQuery()
 * @method static Builder|Variables query()
 * @mixin Eloquent
 * @mixin IdeHelperVariables
 */
class Variables extends Model
{
    use DateMutator;

    const GUARD_NAME = 'web',
        ROLE_ADMIN = 'admin',
        ROLE_CLIENT = 'client',
        SA = 'super_admin', // all right
        NOT_ADMIN = 0,
        DEFAULT_STATUS = 1,
        default_date_format = 'DD.MM.YYYY',
        default_datetime_format = 'DD.MM.YYYY - HH:mm';

    public static array $fullRoles = [
        self::ROLE_ADMIN,
        self::ROLE_CLIENT,
    ];

    const DEFAULT_SA_EMAILS = [
        'info@siberfx.com' => '47819812',
    ]; // default admin account

    /**
     * @var array|string[]
     */
    public static array $fullRolesSelector = [
        self::ROLE_ADMIN => 'Yönetici',
        self::ROLE_CLIENT => 'Firma Hesabı',
    ];

    public static array $fullPermissions = [

        'crm_setting' => [self::ROLE_ADMIN],

        'permission_manager' => [self::ROLE_ADMIN],

        'project_access' => [self::ROLE_ADMIN],
        'project_show' => [self::ROLE_ADMIN],
        'project_edit' => [self::ROLE_ADMIN],
        'project_create' => [self::ROLE_ADMIN],
        'project_delete' => [self::ROLE_ADMIN],

        'dashboard' => [self::ROLE_ADMIN, self::ROLE_CLIENT],
        'company-list' => [self::ROLE_ADMIN],
        'company-preview' => [self::ROLE_ADMIN],
        'company-create' => [self::ROLE_ADMIN],
        'company-update' => [self::ROLE_ADMIN],
        'company-delete' => [self::ROLE_ADMIN],

        'employee_access' => [self::ROLE_ADMIN],
        'employee_show' => [self::ROLE_ADMIN],
        'employee_create' => [self::ROLE_ADMIN],
        'employee_edit' => [self::ROLE_ADMIN],
        'employee_delete' => [self::ROLE_ADMIN],

        'media_access' => [self::ROLE_ADMIN],
        'media_show' => [self::ROLE_ADMIN],
        'media_create' => [self::ROLE_ADMIN],
        'media_edit' => [self::ROLE_ADMIN],
        'media_delete' => [self::ROLE_ADMIN],


        'user_access' => [self::ROLE_ADMIN],
        'user_show' => [self::ROLE_ADMIN],
        'user_create' => [self::ROLE_ADMIN],
        'user_edit' => [self::ROLE_ADMIN],
        'user_delete' => [self::ROLE_ADMIN],


        self::SA => [],

    ];

    /**
     * @var array|string[]
     */
    public static array $months = [
        1 => 'Jan',
        2 => 'Feb',
        3 => 'Mar',
        4 => 'Apr',
        5 => 'May',
        6 => 'Jun',
        7 => 'Jul',
        8 => 'Aug',
        9 => 'Sep',
        10 => 'Oct',
        11 => 'Nov',
        12 => 'Dec',
    ];
}
