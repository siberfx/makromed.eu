<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

/**
 * App\Models\NotificationTemplate
 *
 * @property int $id
 * @property string $name
 * @property string $model
 * @property string $body
 * @property string $slug
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|NotificationTemplate newModelQuery()
 * @method static Builder|NotificationTemplate newQuery()
 * @method static Builder|NotificationTemplate query()
 * @method static Builder|NotificationTemplate whereBody($value)
 * @method static Builder|NotificationTemplate whereCreatedAt($value)
 * @method static Builder|NotificationTemplate whereId($value)
 * @method static Builder|NotificationTemplate whereModel($value)
 * @method static Builder|NotificationTemplate whereName($value)
 * @method static Builder|NotificationTemplate whereSlug($value)
 * @method static Builder|NotificationTemplate whereUpdatedAt($value)
 * @mixin Eloquent
 * @mixin IdeHelperNotificationTemplate
 */
class NotificationTemplate extends BaseModel
{
    use CrudTrait;

    protected $table = 'notification_templates';

     protected $guarded = [];

    /**
     * @var string[]
     */
     public static array $availableModels = [
         'User' => 'Models\User',
         'Ticket' => 'Models\Ticket',
         'Hosting' => 'Models\Hosting',
         'Appointment' => 'Models\Appointment',
     ];
}
