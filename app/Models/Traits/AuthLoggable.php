<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Siberfx\AuthenticationLogger\Models\AuthLogger;

trait AuthLoggable
{
    public function authentications(): MorphMany
    {
        return $this->morphMany(AuthLogger::class, 'authenticatable')->latest('login_at');
    }

    public function notifyAuthenticationLogVia(): array
    {
        return ['telegram'];
    }

    public function lastLoginAt(): string
    {
        $date = optional($this->authentications()->first())->login_at;

        return DateMutator::mutateDate($date);
    }

    public function lastSuccessfulLoginAt(): string
    {
        $date = optional($this->authentications()->whereLoginSuccessful(true)->first())->login_at;

        return DateMutator::mutateDate($date);
    }

    public function lastLoginIp()
    {
        return optional($this->authentications()->first())->ip_address;
    }

    public function lastSuccessfulLoginIp()
    {
        return optional($this->authentications()->whereLoginSuccessful(true)->first())->ip_address;
    }

    public function previousLoginAt(): string
    {
        $date = optional($this->authentications()->skip(1)->first())->login_at;

        return DateMutator::mutateDate($date);

    }

    public function previousLoginIp()
    {
        return optional($this->authentications()->skip(1)->first())->ip_address;
    }
}
