<?php

namespace App\Models\Traits;

use App\Models\SystemLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait AuditableTrait
{

    /**
     * @return MorphMany
     */
    public function system_log(): MorphMany
    {
        return $this->morphMany(SystemLog::class, 'entity');

    }

    public static function bootAuditableTrait()
    {
        static::created(function (Model $model) {
            self::audit('created', $model);
        });

        static::deleted(function (Model $model) {
            self::audit('deleted', $model);
        });

        static::restored(function (Model $model) {
            self::audit('restored', $model);
        });
    }

    /**
     * @param $description
     * @param $model
     */
    protected static function audit($description, $model)
    {
        $generateDesc = self::LOGMODELKEY;

        $desc = $model->{$generateDesc} . ' : ' . $description;

        SystemLog::create([
            'action' => $description,
            'description' => $desc,
            'entity_id' => $model->id ?? null,
            'entity_type' => get_class($model) ?? null,
            'user_id' => backpack_user()->getAuthIdentifier() ?? null,
            'event_date' => now(),
            'ip_address' => request()->ip() ?? null,
        ]);

    }
}
