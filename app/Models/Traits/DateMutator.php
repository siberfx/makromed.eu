<?php

namespace App\Models\Traits;

use Carbon\Carbon;

trait DateMutator
{
    /**
     * @param null $date
     * @param bool $withTime
     * @return string
     */
    public static function mutateDate($date = null, bool $withTime = true): string
    {
        $mutatedDate = Carbon::parse($date)->locale(config('app.locale'));

        $dateString = (true == $withTime)
            ? $mutatedDate->isoFormat(self::default_datetime_format)
            : $mutatedDate->isoFormat(self::default_date_format);

        return sprintf('%s', $dateString);
    }

}
