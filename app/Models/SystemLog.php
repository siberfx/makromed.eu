<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * @mixin IdeHelperSystemLog
 */
class SystemLog extends BaseModel
{

    protected $table = 'system_logs';

    protected $guarded = [];


    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return false|string
     */
    public function getEntityNameAttribute()
    {
        return substr($this->entity_type, 11);
    }
}
