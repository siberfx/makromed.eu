<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperSlider
 */
class Slider extends BaseModel
{
    use HasTranslations;

    protected $translatable = ['title'];

    protected $table = 'sliders';

    protected $fillable = [
        'sort',
        'title',
        'path',
        'status',
    ];

    public static function scopeActive()
    {
        return self::where('status', StatusModel::ACTIVE)->orderBy('sort', 'asc');
    }

}
