<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @mixin IdeHelperCategory
 */
class Category extends BaseModel
{
    use Sluggable;
    use SluggableScopeHelpers;
    use HasTranslations;

    protected $translatable = ['title', 'content', 'additional'];

    protected $table = 'categories';

    protected $fillable = [
        'sort',
        'status',
        'title',
        'content',
        'slug',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    public function scopeActive($query)
    {
        return $query->where('status', Variables::DEFAULT_STATUS)
            ->orderBy('sort', 'asc');
    }


    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    /**
     * @return mixed
     */
    public function getProductCount()
    {
        return $this->products->count() > 0 ? $this->products()->active()->count() : 0;
    }
}
