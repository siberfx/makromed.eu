<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .navsep {color: #999999;}
        img {border:0px;}
        a:link {color:#000;}
    </style>
</head>
<body marginheight="0" topmargin="0" marginwidth="0" leftmargin="0" style="margin: 0px; background-color: #ffffff; background-repeat: repeat; padding: 0px;" bgcolor="#ffffff">
<table width="100%" border="0" cellspacing="0" cellpadding="20">
    <tr>
        <td>
            <table width="666" border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse:separate; border-spacing:0;">
                <tbody>
                <tr>
                    <td align="left" valign="top" style="background-color:#ffffff;">
                        <table width="666" border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse:separate; border-spacing:0; margin-bottom:16px;">
                            <tbody>
                            <tr>
                                <!-- Logo Bar -->
                                <td width="350" align="left">
                                    <img src="https://atlantikyazilim.com/wp-content/uploads/2017/07/logo_kucuk.png" alt="Logo" />
                                </td>
                                <td align="right">
                                    <table width="200" border="0" cellspacing="0" cellpadding="3">
                                        <tr>
                                            <td align="right" style="font-size:12px; font-family: Arial, Helvetica, sans-serif; color: #999999;">Atlantik Yazılım</td>
                                            <td width="20" rowspan="2" align="right" style="font-size:12px; font-family: Arial, Helvetica, sans-serif; color: #999999;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-size:12px; font-family: Arial, Helvetica, sans-serif; color: #000000; text-transform: capitalize;">
                                                Bilgilendirme
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- Nav Bar -->
