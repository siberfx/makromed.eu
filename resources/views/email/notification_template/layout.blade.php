@include('email.notification_template.header')
<table width="666" border="0" cellspacing="0" cellpadding="0" style="border-collapse:separate; border-spacing:0;">

    <tr>
        <td align="left" valign="top">
            <table width="660" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" style="color:#666666; font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 20px; margin: 0; padding: 5px 0;">
                        {!! $body !!}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">&nbsp;</td>
    </tr>
</table>
@include('email.notification_template.footer')
