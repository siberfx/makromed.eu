@extends('layouts.login-app')
@section('title', __('global.reset_password'))

@section('content')
    <div class="content">

        <form autocomplete="off" action="{{ route('password.email') }}" method="post" name="loginform" id="loginform" class="login-form">
            @csrf
            <div id="content">
                <div class="form-group">
                    <label for="username">
                        Enter your email
                    </label>
                    <input
                        class="form-control form-control--username {{ $errors->has('email') ? ' is-invalid' : '' }}"
                        type="text"
                        name="email"
                        id="email"
                        required
                        autocomplete="email"
                        autofocus
                        value="{{ old('email') }}"
                    />


                    <em class="form-control-icon form-control-icon--username"></em>
                </div>

                <button type="submit" class="btn btn-primary" id="loginButton">
                    {{ trans('global.send_password') }}
                </button>
            </div>

            <footer class="content-footer">
                <nav class="footer-navigation">
                    @if(Route::has('login'))
                        <a class="footer-navigation-item" href="{{ route('login') }}">
                            {{ trans('global.login') }}
                        </a>
                    @endif

                    @if(Route::has('register'))

                        <div class="footer-navigation-item">
                            No account yet?
                            <a href="{{ route('register') }}">
                                {{ __('global.forgot_password') }}
                            </a>
                        </div>
                    @endif

                </nav>
            </footer>
        </form>
    </div>
@endsection

