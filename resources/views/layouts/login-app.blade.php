@php
    $setting = \App\Models\Settings\Setting::firstOrFail();
@endphp
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="x-dns-prefetch-control" content="on">
    <link rel="dns-prefetch" href="//c.dcdn.eu">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="theme-color" content="{{ $setting->primary_color ?? '#E30613' }}">
    <link rel="icon" href="/{{ $setting->favicon ?? 'favicon.png' }}" sizes="32x32"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/legacyLogin.css') }}" />

    <title>@yield('title') || {{ $setting->name }}</title>

</head>
<body>

<div id="app">
    <aside class="sidebar" style="background: red url('/{{ $setting->path ?? 'siberfx.jpg' }}') no-repeat 100% 100%">
        <div class="slogan-container">
            <h1 class="slogan-title">
                {{ $setting->name }}
            </h1>
            <h2 class="slogan-subtitle">
                #CRM Portalı
            </h2>
        </div>
    </aside>
    <main class="main">
        @yield('content')
    </main>
</div>

</body>
</html>
