<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{ asset('uploads/logo/favicon.png') }}" sizes="32x32"/>
    <!-- Place favicon.ico in the root directory -->
    @include('layouts.partial._stylesheets')

    <title>@yield('title')</title>

</head>
<body>
<div id="preloader">
    <img src="/img/logo/preloader.gif" alt="">
</div>

<button class="scroll-top scroll-to-target" data-target="html">
    <i class="fas fa-angle-up"></i>
</button>
<x-frontend.header />

@yield('content')

@include('layouts.partial._javascripts')
<x-frontend.footer />
</body>
</html>
