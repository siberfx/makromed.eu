@extends(backpack_view('blank'))

@php
    // ---------------------
    // JUMBOTRON widget demo
    // ---------------------
    // Widget::add([
 //        'type'        => 'jumbotron',
 //        'name' 		  => 'jumbotron',
 //        'wrapperClass'=> 'shadow-xs',
 //        'heading'     => trans('backpack::base.welcome'),
 //        'content'     => trans('backpack::base.use_sidebar'),
 //        'button_link' => backpack_url('logout'),
 //        'button_text' => trans('backpack::base.logout'),
 //    ])->to('before_content')->makeFirst();

    // -------------------------
    // FLUENT SYNTAX for widgets
    // -------------------------
    // Using the progress_white widget
    //
    // Obviously, you should NOT do any big queries directly in the view.
    // In fact, it can be argued that you shouldn't add Widgets from blade files when you
    // need them to show information from the DB.
    //
    // But you do whatever you think it's best. Who am I, your mom?
    $companyCount = new \App\Models\Company;

     // notice we use Widget::add() to add widgets to a certain group
    Widget::add()->to('before_content')->type('div')->class('row')->content([
        // notice we use Widget::make() to add widgets as content (not in a group)

    ]);



@endphp

@section('content')
    {{-- In case widgets have been added to a 'content' group, show those widgets. --}}
    @include(backpack_view('inc.widgets'), [ 'widgets' => app('widgets')->where('group', 'content')->toArray() ])

@endsection
