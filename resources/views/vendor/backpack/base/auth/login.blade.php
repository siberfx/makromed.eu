@extends('layouts.login-app')
@php
    $setting = \App\Models\Settings\Setting::first() ?? [];
@endphp
@section('title', $setting->name ?? __('base.login'))

@section('content')
    <div class="content">
        <header class="content-header">
            <div class="logo" style="background-image: url(/{{ $setting->logo ?? 'small-logo.png' }});">
                <span class="text-hide">{{ config('app.name') }}</span>
            </div>
            <span class="cta-title">
                    Hesabınıza Giriş yapın
                </span>
        </header>
        <form autocomplete="off" action="{{ route('backpack.auth.login') }}" method="post" name="loginform" id="loginform" class="login-form">
            @csrf
            <input type="hidden" name="ip_address" value="{{ request()->getClientIp() }}">
            <div id="content">
                <div class="form-group">
                    <label for="username">
                        E-posta adresiniz
                    </label>
                    <input
                        class="form-control form-control--username {{ $errors->has('email') ? ' is-invalid' : '' }}"
                        type="text"
                        name="email"
                        id="email"
                        required
                        autocomplete="email"
                        autofocus
                        value="{{ old('email', null) }}"
                    />
                    <em class="form-control-icon form-control-icon--username"></em>


                </div>
                <div class="form-group">
                    <label for="password">
                        Şifreniz
                    </label>
                    <input
                        autocomplete="off"
                        id="password"
                        name="password"
                        type="password"
                        class="form-control form-control--password {{ $errors->has('password') ? ' is-invalid' : '' }}"
                    />
                    <em class="form-control-icon form-control-icon--password"></em>

                </div>

                <p id="errormessage" class="ahoy-margin-bottom-4 ahoy-margin-top-4 ahoy-text-body ahoy-text-color-ruby-dark red">
                    @if ($errors->has('password'))
                        {{ $errors->first('password') }}
                    @endif
                    @if ($errors->has($username))
                        {{ $errors->first($username) }}
                    @endif
                </p>


                <button type="submit" class="btn btn-primary" style="background-color: {{ $setting->primary_color ?? 'red' }}" id="loginButton">
                    {{ __('base.login') }}
                </button>
            </div>


            <footer class="content-footer">
                <nav class="footer-navigation">
                        <a class="footer-navigation-item" href="{{ route('backpack.auth.password.reset') }}">
                            {{ __('base.forgot_your_password') }}
                        </a>

                    @if(Route::has('register'))

                        <div class="footer-navigation-item">
                            No account yet?
                            <a href="{{ route('backpack.auth.register') }}">
                                {{ __('base.register') }}
                            </a>
                        </div>
                    @endif

                </nav>
            </footer>
        </form>
    </div>
@endsection
