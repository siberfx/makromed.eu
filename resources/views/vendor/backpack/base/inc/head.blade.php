@php
    $setting = \App\Models\Settings\Setting::first() ?? [];
@endphp

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<link rel="icon" href="/{{ $setting->favicon }}" type="image/x-icon" />
@if (config('backpack.base.meta_robots_content'))<meta name="robots" content="{{ config('backpack.base.meta_robots_content', 'noindex, nofollow') }}"> @endif

<meta name="csrf-token" content="{{ csrf_token() }}" /> {{-- Encrypted CSRF token for Laravel, in order for Ajax requests to work --}}
<title>{{ isset($title) ? $title.' :: '.config('backpack.base.project_name') : config('backpack.base.project_name') }}</title>

@yield('before_styles')
@stack('before_styles')
<style>
    :root{
        --defined-primary: {{ $setting->primary_color ?? '#f16f58' }};
        --defined-primary-hover: {{ $setting->primary_color_hover ?? '#893e37' }};
        --defined-bg: {{ $setting->primary_header_bg ?? '#443789' }};
        --defined-text: {{ $setting->primary_header_text ?? '#f1f1f1' }};

        --blue:#467fd0;
        --indigo:#6610f2;
        --purple:#7c69ef;
        --pink:#e83e8c;
        --red:#df4759;
        --orange:#fd9644;
        --yellow:#ffc107;
        --green:#42ba96;
        --teal:#20c997;
        --cyan:#17a2b8;
        --white:#fff;
        --gray:#869ab8;
        --gray-dark:#384c74;
        --light-blue:#69d2f1;
        --primary:#7c69ef;
        --secondary:#d9e2ef;
        --success:#42ba96;
        --info:#467fd0;
        --warning:#ffc107;
        --danger:#df4759;
        --light:#f1f4f8;
        --dark:#161c2d;
        --default:#d9e2ef;
        --notice:#467fd0;
        --error:#df4759;
        --breakpoint-xs:0;
        --breakpoint-sm:576px;
        --breakpoint-md:768px;
        --breakpoint-lg:992px;
        --breakpoint-xl:1200px;
        --font-family-sans-serif:"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
        --font-family-monospace:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace
    }
</style>

@if (config('backpack.base.styles') && count(config('backpack.base.styles')))
    @foreach (config('backpack.base.styles') as $path)
        <link rel="stylesheet" type="text/css" href="{{ asset($path).'?v='.config('backpack.base.cachebusting_string') }}">
    @endforeach
@endif

@if (config('backpack.base.mix_styles') && count(config('backpack.base.mix_styles')))
    @foreach (config('backpack.base.mix_styles') as $path => $manifest)
        <link rel="stylesheet" type="text/css" href="{{ mix($path, $manifest) }}">
    @endforeach
@endif

@yield('after_styles')

<style>

    /* Button used to open the chat form - fixed at the bottom of the page */
    .open-button {
        background-color: #555;
        line-height: 35px;
        color: white;
        border: none;
        cursor: pointer;
        opacity: 0.8;
        position: fixed;
        bottom: 50px;
        border-radius: 4px;
        right: 40px;
        width: 100px;
    }

    /* The popup chat - hidden by default */
    .ticket-popup {
        display: none;
        position: fixed;
        bottom: 0;
        right: 15px;
        border: 3px solid #f1f1f1;
        z-index: 9;
    }

    #create-ticket{
        top: 4%;
    }

</style>
@stack('after_styles')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
