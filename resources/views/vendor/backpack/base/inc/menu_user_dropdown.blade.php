<li class="nav-item dropdown pr-4 d-none d-sm-block">
    <span style="color:mintcream; "> <i class="la la-tasks"></i> Ver : {!! config('version.string') !!}</span>
</li>
<li class="nav-item dropdown pr-4">
    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <img class="img-avatar" src="{{ backpack_avatar_url(backpack_auth()->user()) }}" alt="{{ backpack_auth()->user()->name }}">
    </a>
    <div class="dropdown-menu {{ config('backpack.base.html_direction') == 'rtl' ? 'dropdown-menu-left' : 'dropdown-menu-right' }} mr-4 pb-1 pt-1">
        <a class="dropdown-item" href="{{ route('backpack.account.info') }}"><i class="la la-user"></i> Hesabı yönet</a>
        <div class="dropdown-divider"></div>
<a class="dropdown-item" href="{{ backpack_url('system-logs') }}"><i class="la la-exchange-alt"></i> Sistem Kayıtları </a>

<div class="dropdown-divider"></div>
        <a class='dropdown-item' href='{{ backpack_url('sent-email') }}'><i class='nav-icon la la-envelope'></i> E-posta geçmişi</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{ backpack_url('logout') }}"><i class="la la-lock"></i> Çıkış yap </a>
    </div>
</li>
