{{-- checkbox with loose false/null/0 checking --}}
@php
    use App\Models\SentEmail;

    $open = SentEmail::OPEN;
    $aborted = SentEmail::ABORTED;
    $succeed = SentEmail::SUCCEED;
    $failed = SentEmail::FAILED;

    $colors = SentEmail::$colors;
    $statuses = SentEmail::$statuses;

    $checkValue = data_get($entry, $column['name']);

    $checkedIcon = data_get($column, 'icons.checked', 'la-check-circle');
    $uncheckedIcon = data_get($column, 'icons.unchecked', 'la-circle');
    $failed = $aborted = data_get($column, 'icons.unchecked', 'la-times');

    $exportCheckedText = data_get($column, 'labels.checked', trans('backpack::crud.yes'));
    $exportUncheckedText = data_get($column, 'labels.unchecked', trans('backpack::crud.no'));

    $icon = $checkValue == $succeed ? $checkedIcon : $uncheckedIcon;
    $color = $colors[$checkValue];

    $column['text'] = $statuses[$checkValue];
    $column['escaped'] = $column['escaped'] ?? true
@endphp

<span>
    @includeWhen(!empty($column['wrapper']), 'crud::columns.inc.wrapper_start')
    <span style="color: {{ $color }}">
        <i class="la {{ $icon }}"></i> {{ $column['text'] }}
    </span>
    @includeWhen(!empty($column['wrapper']), 'crud::columns.inc.wrapper_end')
</span>

<span class="sr-only">
    {!! $column['text'] !!}
</span>
