{{-- converts 1/true or 0/false to yes/no/lang --}}
@php
    $value = data_get($entry, $column['name']);

    $column['clean'] = strip_tags($value);

    $column['escaped'] = $column['escaped'] ?? true;
    $column['text'] = is_null($value) ? '-' : $value;

@endphp

<span data-order="{{ $entry->getKey() }}" id="show-{{ $entry->getKey() }}">
	@includeWhen(!empty($column['wrapper']), 'crud::columns.inc.wrapper_start')
    <span style="cursor: pointer" data-password="{{ $value }}" onclick="showPassword(this);" id="show-password-{{ $entry->getKey() }}"> <i class="la la-eye"></i> </span>
    @includeWhen(!empty($column['wrapper']), 'crud::columns.inc.wrapper_end')
</span>
