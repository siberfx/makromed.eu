@if($entry->status_id == \App\Models\Ticket::TICKET_ACTIVE)
<a
    href="{{ url('ticket/'.$entry->getKey().'/action') }}"
    style="color:black !important; font-weight: bold" class="btn btn-sm btn-link">
    <i class="la la-times"></i>
    Talebi Kapat
</a>
@else
    <span style="color:darkgray">
        <i class="la la-check"></i>
        Kapatılmış
    </span>
@endif
