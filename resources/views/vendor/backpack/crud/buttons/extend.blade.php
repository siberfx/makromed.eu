@if ($crud->hasAccess('list'))
    <a onclick="return confirm('1 yıl uzatma işlemini onaylıyor musun?')" href="{{ url($crud->route.'/'.$entry->getKey().'/extend') }} "
       class="btn btn-sm btn-link"><i class="la la-calendar-times"></i> Uzat</a>
@endif
