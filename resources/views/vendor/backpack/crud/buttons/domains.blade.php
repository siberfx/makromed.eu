@if ($crud->hasAccess('show'))
	<!-- Single edit button -->
	<a href="{{ url($crud->route.'/domain-list/'.$entry->getKey()) }}" class="btn btn-sm btn-link"><i class="la la-globe"></i> Listele</a>
@endif
