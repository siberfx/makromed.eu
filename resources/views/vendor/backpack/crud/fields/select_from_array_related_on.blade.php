<!-- select from array -->
@include('crud::fields.inc.wrapper_start')
@php
$value = $field['value'] ?? '0';
$ajaxCallId = $field['related_on_key'];
@endphp
<label>{!! $field['label'] !!}</label>
@include('crud::fields.inc.translatable_icon')
<select
    name="{{ $field['name'] }}@if (isset($field['allows_multiple']) && $field['allows_multiple']==true)[]@endif"
    id="{{ $field['name'] }}-ajax"
    @include('crud::fields.inc.attributes')
@if (isset($field['allows_multiple']) && $field['allows_multiple']==true)multiple @endif
id="{{ $field['name'] }}-ajax"
>
</select>

{{-- HINT --}}
@if (isset($field['hint']))
<p class="help-block">{!! $field['hint'] !!}</p>
@endif
@include('crud::fields.inc.wrapper_end')


{{-- FIELD JS - will be loaded in the after_scripts section --}}
@push('crud_fields_scripts')
<!-- include select2 js-->
<script>
    let optionList = $("#{{ $field['name'] }}-ajax");
    let activeValue = '{{ $value }}';
    let selectField = $('select[name={{$ajaxCallId}}]');

    selectField.val() ? makeRequestQuery(selectField.val()) : '';

    selectField.on('change', function() {
        makeRequestQuery(selectField.val())

    });


    function makeRequestQuery(companyId) {
        console.log('New value is: ' + companyId);

        $.ajax({
            url: '{{ $field['related_route'] }}',
            type: 'POST',
            data: {
                _token: '{{ csrf_token() }}',
                companyId: companyId,
            },
            dataType: 'json',

            success: function (response) {
                optionList.empty();
                $.each(response, function (key, value) {
                    console.log(value)
                    let renderHtml = "<option value='" + value.id + "'>" + value.full_name_with_email + "</option>"
                    optionList.append(renderHtml);
                });

                if (activeValue !== '0') {
                    optionList.val(activeValue);
                }
            }
        });
    }

</script>

@endpush

{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
