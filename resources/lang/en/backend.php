<?php

return [

    'dashboard' => 'Stats',
    'filemanager' => 'File Manager',
    'users_and_permission' => 'Users & Permission',
    'users' => 'Users',
    'roles' => 'Roles',
    'permissions' => 'Permissions',
    'categories' => 'Categories',
    'settings' => 'General Settings',
    'error_logs' => 'Error Logs',
    'admin_actions' => 'User Actions',
    'statistics' => 'Stats',
    'notifications' => 'Templates',

    'title' => ' Title',
    'content' => ' Content',
    'address' => ' Address',
    'order' => 'Sort',
    'phone' => 'Phone',
    'fax' => 'Fax',
    'mobile' => 'Mobile Phone',
    'email' => 'Email',
    'logo' => 'Logo',
    'favicon' => 'Favicon',
    'archived' => 'Arhived',

    'status' => [
        'title' => 'Status',
        'active' => 'Open',
        'inactive' => 'Closed',
    ],
    'positions' => [
        'title' => 'Position',
        'header' => 'before &lt;/head&gt;',
        'footer' => 'before &lt;/body&gt;',
    ],
    'tabs' => [
        'general' => 'General',
        'properties' => 'Specification',
        'contacts' => 'Contacts',
        'custom-fields' => 'Custom Fields',
        'details' => 'Details',
        'images' => 'Images',
    ],
    'category' => 'Category',
    'parent' => 'Parent',

    'block' => 'Block',

    'position' => 'Position',
    'added_at' => 'Added at',
    'created_at' => 'Created at',
    'updated_at' => 'Updated at',

    'avatar' => 'Avatar',
    'username' => 'Username',
    'password' => 'Password',

    'php' => 'Php Version',
    'mysql' => 'MySQL Version',
    'hdd' => 'HDD Space',
    'version' => 'Version info',

    'ip_address' => 'IP Address',
    'occurred_at' => 'Occurred at',
    'administrator' => 'Administrator',
    'related_page' => 'Related page',
    'action' => 'Action',
];
