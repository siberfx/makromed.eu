<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Şifreniz en az 6 karakter olmak zorundadır ve eşleşmelidir.',
    'reset' => 'Şifreniz sıfırlanmıştır!',
    'sent' => 'Yeni şifrenizi tarafınıza mail olarak gönderdik.',
    'token' => 'Şifre sıfırlama kodunuz geçersiz.',
    'user' => "Bu e-posta adresine bağlı bir kullanıcı bulunamamıştır.",

];
