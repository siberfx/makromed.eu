<?php

return [

    'corporate' => 'Kurumsal',
    'portfolio' => 'Neler yaptık',
    'reference' => 'Referanslar',
    'service' => 'Hizmetlerimiz',
    'blog' => 'Blog',
    'contact-us' => 'İletişim',

    'corporate-hover' => 'Hakkında',
    'portfolio-hover' => 'Projelerimiz',
    'reference-hover' => 'Referanslar',
    'service-hover' => 'Servislerimiz',
    'blog-hover' => 'Blog',
    'contact-hover' => 'İletişim',

];
