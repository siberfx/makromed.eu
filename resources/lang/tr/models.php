<?php

return [
    'article' => 'Makale',
    'page' => 'Sayfa',
    'article_category' => 'Makale Kategori',
    'portfolio' => 'Portfolio',
    'settings' => 'Site Ayar',
    'slider' => 'Slider',
    'users' => 'User',
];
