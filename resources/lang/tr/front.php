<?php

return [

// Local Trabslations
    "lang" 				=> "tr",
    "langCode" 			=> "tr_TR",
    "Lingual" 			=> "Dil",

    "menu" 				=> [
        "main" 			=> "Ana Sayfa",
        "products" 	    => "Portföy",
        "about-us" 	    => "Kurumsal",
        "policy" 	    => "Policy",
        "contact" 	    => "İletişim",
    ],

// Form bölümü
    "form" => [
        "name" 		    => "Tam adınız :",
        "address" 		=> "Adres :",
        "phone" 		=> "Telefon :",
        "email" 		=> "E-posta :",
        "message" 		=> "Mesajınız :",
        "media" 		=> "Sosyal Ağlar",
        "web" 			=> "Web",
        "quicklinks" 	=> "Hızlı Linkler",
        "readmore" 		=> "Devamı",
    ],

    "footer" => [
        "keywords" 		=> "Kelimeler",
        "contact" 		=> "İletişim",
        "links" 		=> "Hızlı Linkler",
        "sitemap" 		=> "Site Haritası",
        "social" 		=> "Sosyal Ağlar",
        "copyright" 	=> "&copy; :date - Tüm hakları saklıdır. | Dizayn: <a href='https://siberfx.com' target='_new'>:name</a>",
    ],

//Form placeholders:

    "contact" 			=> [
        "info"			=> "Contact Us details are below",
        "office" 		=> "Şubelerimiz",
        "name" 			=> "Fullname",
        "phone" 		=> "Phone number",
        "mail" 			=> "Mail address",
        "subject" 		=> "Konu",
        "message" 		=> "Yorum",
        "send" 			=> "Formu gönder",
        "getintouch" 	=> "iletişime <span>geçin</span>",
        "giveusmessage"	=> "Give us a <span>Message</span>",
        "success"		=> "Thank you! we'll contact you shortly.",
        "getdirection"	=> "Get <span>Directions</span>",
    ],

    // Properties
    "property" 			=> [
        "publish_date" 		=> "Yayın tarihi",
        "framework" 		=> "Framework",
        "php" 		        => "Php versiyon",
        "team_size" 		=> "Çalışan sayısı",
        "bootstrap" 		=> "Bootstrap",
        "vuejs" 		    => "VueJs",
        "repo" 		        => "Proje kaynağı",
        "technology" 		=> "Teknolojiler",
    ],

    'packages' => 'Plans',
    'maintenance' => 'Sorun sende değil, bende :)',
];
