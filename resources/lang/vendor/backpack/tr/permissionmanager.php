<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Permission Manager Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Laravel Backpack - Permission Manager
    | Author: Lúdio Oliveira <ludio.ao@gmail.com>
    |
    */
    'name'                  => 'İsim',
    'role'                  => 'Rol',
    'roles'                 => 'Roller',
    'roles_have_permission' => 'Bu yetkinin yapabildikleri',
    'permission_singular'   => 'yetki',
    'permission_plural'     => 'yetkiler',
    'user_singular'         => 'Yönetici',
    'user_plural'           => 'Yöneticiler',
    'email'                 => 'E-posta',
    'extra_permissions'     => 'Ek yetkiler',
    'password'              => 'Şifre',
    'password_confirmation' => 'Şifre (tekrar)',
    'user_role_permission'  => 'Yönetici ve Yetkileri',
    'user'                  => 'Yönetici',
    'users'                 => 'Yöneticiler',
    'guard_type'            => 'Guard Tipi',

];
