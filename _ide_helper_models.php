<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\BaseModel
 *
 * @method static Builder|BaseModel newModelQuery()
 * @method static Builder|BaseModel newQuery()
 * @method static Builder|BaseModel query()
 * @mixin Eloquent
 * @mixin IdeHelperBaseModel
 */
	class IdeHelperBaseModel extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Category
 *
 * @mixin IdeHelperCategory
 * @property int $id
 * @property int $sort
 * @property int $status
 * @property array $title
 * @property array|null $content
 * @property string|null $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $slug_or_title
 * @property-read array $translations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|Category active()
 * @method static \Illuminate\Database\Eloquent\Builder|Category findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category withUniqueSlugConstraints(\Illuminate\Database\Eloquent\Model $model, string $attribute, array $config, string $slug)
 */
	class IdeHelperCategory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Note
 *
 * @property int $id
 * @property int $ticket_id
 * @property int|null $user_id
 * @property string $note
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Note newModelQuery()
 * @method static Builder|Note newQuery()
 * @method static Builder|Note query()
 * @method static Builder|Note whereCreatedAt($value)
 * @method static Builder|Note whereId($value)
 * @method static Builder|Note whereNote($value)
 * @method static Builder|Note whereTicketId($value)
 * @method static Builder|Note whereUpdatedAt($value)
 * @method static Builder|Note whereUserId($value)
 * @mixin Eloquent
 * @mixin IdeHelperNote
 */
	class IdeHelperNote extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\NotificationTemplate
 *
 * @property int $id
 * @property string $name
 * @property string $model
 * @property string $body
 * @property string $slug
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|NotificationTemplate newModelQuery()
 * @method static Builder|NotificationTemplate newQuery()
 * @method static Builder|NotificationTemplate query()
 * @method static Builder|NotificationTemplate whereBody($value)
 * @method static Builder|NotificationTemplate whereCreatedAt($value)
 * @method static Builder|NotificationTemplate whereId($value)
 * @method static Builder|NotificationTemplate whereModel($value)
 * @method static Builder|NotificationTemplate whereName($value)
 * @method static Builder|NotificationTemplate whereSlug($value)
 * @method static Builder|NotificationTemplate whereUpdatedAt($value)
 * @mixin Eloquent
 * @mixin IdeHelperNotificationTemplate
 */
	class IdeHelperNotificationTemplate extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Page
 *
 * @mixin IdeHelperPage
 * @property int $id
 * @property int $status
 * @property array $title
 * @property array|null $content
 * @property string|null $google_title
 * @property string|null $google_keyword
 * @property string|null $google_description
 * @property string|null $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read array $translations
 * @method static \Illuminate\Database\Eloquent\Builder|Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereGoogleDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereGoogleKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereGoogleTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUpdatedAt($value)
 */
	class IdeHelperPage extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Product
 *
 * @mixin IdeHelperProduct
 * @property int $id
 * @property int $category_id
 * @property int $sort
 * @property int $status
 * @property array $title
 * @property array|null $content
 * @property array|null $additional
 * @property string|null $pdf
 * @property string|null $google_title
 * @property string|null $google_keyword
 * @property string|null $google_description
 * @property string|null $data
 * @property string|null $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Category $category
 * @property-read array $translations
 * @property-write mixed $path
 * @method static \Illuminate\Database\Eloquent\Builder|Product active()
 * @method static \Illuminate\Database\Eloquent\Builder|Product findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereAdditional($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereGoogleDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereGoogleKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereGoogleTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product wherePdf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product withUniqueSlugConstraints(\Illuminate\Database\Eloquent\Model $model, string $attribute, array $config, string $slug)
 */
	class IdeHelperProduct extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SentEmail
 *
 * @property int $id
 * @property string|null $to
 * @property string|null $cc
 * @property string|null $subject
 * @property string|null $body
 * @property int|null $sent
 * @property string|null $planned_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|SentEmail newModelQuery()
 * @method static Builder|SentEmail newQuery()
 * @method static Builder|SentEmail query()
 * @method static Builder|SentEmail whereBody($value)
 * @method static Builder|SentEmail whereCc($value)
 * @method static Builder|SentEmail whereCreatedAt($value)
 * @method static Builder|SentEmail whereId($value)
 * @method static Builder|SentEmail wherePlannedAt($value)
 * @method static Builder|SentEmail whereSent($value)
 * @method static Builder|SentEmail whereSubject($value)
 * @method static Builder|SentEmail whereTo($value)
 * @method static Builder|SentEmail whereUpdatedAt($value)
 * @mixin Eloquent
 * @mixin IdeHelperSentEmail
 */
	class IdeHelperSentEmail extends \Eloquent {}
}

namespace App\Models\Settings{
/**
 * App\Models\Settings\Setting
 *
 * @mixin IdeHelperSetting
 * @property int $id
 * @property string $name
 * @property string|null $location
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $mobile
 * @property string|null $fax
 * @property string|null $website
 * @property string|null $logo
 * @property string|null $favicon
 * @property string|null $path
 * @property string|null $primary_color
 * @property string|null $primary_color_hover
 * @property string|null $primary_header_bg
 * @property string|null $primary_header_text
 * @property int|null $notify_telegram
 * @property string|null $telegram_api_key
 * @property string|null $telegram_chat_id 100 must be added before
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read string $mutate_primary_color
 * @property-read string $mutate_primary_color_hover
 * @method static Builder|Setting newModelQuery()
 * @method static Builder|Setting newQuery()
 * @method static Builder|Setting query()
 * @method static Builder|Setting whereCreatedAt($value)
 * @method static Builder|Setting whereEmail($value)
 * @method static Builder|Setting whereFavicon($value)
 * @method static Builder|Setting whereFax($value)
 * @method static Builder|Setting whereId($value)
 * @method static Builder|Setting whereLocation($value)
 * @method static Builder|Setting whereLogo($value)
 * @method static Builder|Setting whereMobile($value)
 * @method static Builder|Setting whereName($value)
 * @method static Builder|Setting whereNotifyTelegram($value)
 * @method static Builder|Setting wherePath($value)
 * @method static Builder|Setting wherePhone($value)
 * @method static Builder|Setting wherePrimaryColor($value)
 * @method static Builder|Setting wherePrimaryColorHover($value)
 * @method static Builder|Setting wherePrimaryHeaderBg($value)
 * @method static Builder|Setting wherePrimaryHeaderText($value)
 * @method static Builder|Setting whereTelegramApiKey($value)
 * @method static Builder|Setting whereTelegramChatId($value)
 * @method static Builder|Setting whereUpdatedAt($value)
 * @method static Builder|Setting whereWebsite($value)
 */
	class IdeHelperSetting extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Slider
 *
 * @mixin IdeHelperSlider
 * @property int $id
 * @property int|null $sort
 * @property array|null $title
 * @property string|null $path
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read array $translations
 * @method static \Illuminate\Database\Eloquent\Builder|Slider active()
 * @method static \Illuminate\Database\Eloquent\Builder|Slider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Slider newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Slider query()
 * @method static \Illuminate\Database\Eloquent\Builder|Slider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slider wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slider whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slider whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slider whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slider whereUpdatedAt($value)
 */
	class IdeHelperSlider extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SystemLog
 *
 * @mixin IdeHelperSystemLog
 * @property int $id
 * @property int $user_id
 * @property string $entity_type
 * @property int $entity_id
 * @property string|null $action
 * @property string|null $description
 * @property string $event_date
 * @property string $ip_address
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read false|string $entity_name
 * @property-read \App\Models\User $user
 * @method static Builder|SystemLog newModelQuery()
 * @method static Builder|SystemLog newQuery()
 * @method static Builder|SystemLog query()
 * @method static Builder|SystemLog whereAction($value)
 * @method static Builder|SystemLog whereCreatedAt($value)
 * @method static Builder|SystemLog whereDescription($value)
 * @method static Builder|SystemLog whereEntityId($value)
 * @method static Builder|SystemLog whereEntityType($value)
 * @method static Builder|SystemLog whereEventDate($value)
 * @method static Builder|SystemLog whereId($value)
 * @method static Builder|SystemLog whereIpAddress($value)
 * @method static Builder|SystemLog whereUpdatedAt($value)
 * @method static Builder|SystemLog whereUserId($value)
 */
	class IdeHelperSystemLog extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @mixin IdeHelperUser
 * @property int $id
 * @property int|null $is_admin
 * @property string $name
 * @property string $email
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Collection|\Siberfx\AuthenticationLogger\Models\AuthLogger[] $authentications
 * @property-read int|null $authentications_count
 * @property-read string $full_name
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read Collection|Role[] $roles
 * @property-read int|null $roles_count
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User permission($permissions)
 * @method static Builder|User query()
 * @method static Builder|User role($roles, $guard = null)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereIsAdmin($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User wherePhone($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 */
	class IdeHelperUser extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Variables
 *
 * @method static Builder|Variables newModelQuery()
 * @method static Builder|Variables newQuery()
 * @method static Builder|Variables query()
 * @mixin Eloquent
 * @mixin IdeHelperVariables
 */
	class IdeHelperVariables extends \Eloquent {}
}

